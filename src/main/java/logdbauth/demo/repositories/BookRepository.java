package logdbauth.demo.repositories;

import logdbauth.demo.models.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {
    boolean existsByNameAndAuthor(String name, String author);
}
