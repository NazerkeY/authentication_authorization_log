package logdbauth.demo.controllers;

import logdbauth.demo.models.User;
import logdbauth.demo.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class RegistrationController {
    @Autowired
    private UserService userService;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/registration")
    public String registration(Model model){
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@RequestParam String email, @RequestParam String username,
                        @RequestParam String password, @RequestParam String confirmPassword,
                        HttpServletRequest request,
                        Model model) {

        if(!password.equals(confirmPassword)){
            model.addAttribute("passwordError", "Your password doesn't match");
            return "registration";
        }
        else {
            if(!userService.checkUser(email)){
                model.addAttribute("emailError", "Account with such email exists!");
                return "registration";
            }
            else {
                userService.saveUser(new User(username, email, password, confirmPassword));
                try {
                    UserDetails userDetails = userService.loadUserByUsername(email);
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
                    if (usernamePasswordAuthenticationToken.isAuthenticated()) {
                        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                        request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
                    }
                } catch (Exception e) {
                }
            }
        }
        return "redirect:/home";
    }
}
