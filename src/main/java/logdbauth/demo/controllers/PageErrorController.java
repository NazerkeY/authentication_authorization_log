package logdbauth.demo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class PageErrorController implements ErrorController {
    private final MessageSource messageSource;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public PageErrorController(MessageSource messageSource){
        this.messageSource = messageSource;
    }


    @RequestMapping("/error")
    public String showError(Model model, HttpServletRequest request, Exception ex){
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {

            int statusCode = Integer.valueOf(status.toString());

            if (statusCode == HttpStatus.FORBIDDEN.value()) {
                return "/error/accessDeniedERROR"; //403
            }

            else if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return "/error/notFoundERROR"; //404
            }

            else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                logger.error("Server doesn't response");
                return "/error/serverERROR"; //500
            }
        }
        return "home";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
