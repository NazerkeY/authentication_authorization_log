package logdbauth.demo.controllers;

import logdbauth.demo.models.Book;
import logdbauth.demo.repositories.BookRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.slf4j.Logger;


@Controller
public class BookController {
    @Autowired
    private BookRepository bookRepository;
    Logger logger = LoggerFactory.getLogger(BookController.class);

    @GetMapping("/books/add")
    public String getBook(Model model){
        return "AddBook";
    }

    @GetMapping("/books")
    public String viewBooks(Model model){
        Iterable<Book> books = bookRepository.findAll();
        model.addAttribute("books", books);
        return "viewBooks";
    }

    @PostMapping("/books/add")
    public String addBook(@RequestParam String name, @RequestParam String author, Model model){
        Iterable<Book> books = bookRepository.findAll();
        for(Book book: books) {
            if (book.getName().equals(name)) {
                if (book.getAuthor().equals(author)) {
                            return "redirect:/books";
                        }
                else { break; }
                } else { break; }
            }
        Book created_book = new Book(name, author);
        bookRepository.save(created_book);

        if(bookRepository.existsByNameAndAuthor(created_book.getName(), created_book.getAuthor())) {
            logger.warn("This book was saved {}", created_book.toString());
        }
        else{
            logger.warn("Book couldn't be added {} ", created_book.toString());
        }

        return "redirect:/books";
    }
}