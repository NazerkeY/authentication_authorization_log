package logdbauth.demo.services;

import logdbauth.demo.models.Role;
import logdbauth.demo.models.User;
import logdbauth.demo.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Collections;
import java.util.List;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if(user != null){
            logger.warn("User logged in {}", user.toString());
            return user;
        }
        else{
            throw new UsernameNotFoundException(String.format("User with %s email cannot be found", email));
        }
    }

    public boolean checkUser(String email){
        User userFromData = userRepository.findByEmail(email);
        if(userFromData != null){
            return false;
        }
        else{
            return true;
        }
    }

    public void saveUser(User user){
        if(checkUser(user.getEmail())){
            user.setRoles(Collections.singleton(new Role("ROLE_USER")));
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userRepository.save(user);
            if(checkUser(user.getEmail())){
                logger.error("User couldn't be added {}", user.getEmail());
            }
            logger.warn("This user was saved {} ", user.toString());
        }
    }

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }
}
